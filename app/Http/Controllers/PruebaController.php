<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function index(){
        return response()->json([
            "saludo" => "Hola mundo laravel 5.4",
        ]);
    }
}
