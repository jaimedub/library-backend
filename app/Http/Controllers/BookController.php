<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Book;


class BookController extends Controller
{
    public function getBooks()
    {
        $books =  DB::table('books')
        ->leftJoin("categories", 'categories.id', '=', 'books.category_id')
        ->select("books.*", "categories.name as category_name")
        ->orderBy('id', 'desc')
        ->get();
        
        return response()->json($books);
    }


    public function getBook($book_id)
    {
        $book =  Book::find($book_id);
        
        return response()->json($book);
    }





    public function postBook(Request $request)
    {
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->published_date = $request->published_date;
        $book->category_id = $request->category_id;
        $book->save();
        
        if($book)
        {
            return response()->json($book);
        }
    }


    public function updateBook(Request $request, $book_id)
    {
        $book = Book::find($book_id);
        
        if($book)
        {
            if($request->has("name"))
            {
                $book->name = $request->name;
            }

            if($request->has("author"))
            {
                $book->author = $request->author;
            }

            if($request->has("published_date"))
            {
                $book->published_date = $request->published_date;
            }
            
            if($request->has("category_id"))
            {
                $book->category_id = $request->category_id;
            }

            $book->save();

            return response()->json($book);
        }
    }



    public function deleteBook(Request $request, $book_id)
    {
        $book = Book::find($book_id);

        if($book)
        {
            $book->delete();
        }

    }



    public function changeStatus(Request $request, $book_id)
    {
        $book = Book::find($book_id);

        $book->available = $request->available;
        
        $book->save();

        return response()->json($book);
    }




    public function search(Request $request){
        $search = $request->get("search");

        $books =  DB::table('books')
        ->leftJoin("categories", 'categories.id', '=', 'books.category_id')
        ->select("books.*", "categories.name as category_name")
        ->where("books.name", "like", "%".$search."%")
        ->orWhere("books.author", "like", "%".$search."%")
        ->orWhere("books.published_date", "like", "%".$search."%")
        ->orWhere("categories.name", "like", "%".$search."%")
        ->orderBy('books.id', 'desc')
        ->get();
        
        return response()->json($books);

    }


}
