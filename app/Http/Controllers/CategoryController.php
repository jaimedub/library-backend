<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Category;

class CategoryController extends Controller
{
    

    public function postCategory(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        
        if($category)
        {
            return response()->json($category);
        }
    }

    
    public function getCategories(){
        $categories =  DB::table('categories')
        ->get();
        
        return response()->json($categories);
    }


}
