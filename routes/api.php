<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post("/search", "BookController@search");

Route::get("/books", "BookController@getBooks");
Route::get("/book/{book_id}", "BookController@getBook");
Route::post("/book", "BookController@postBook");
Route::put("/book/{book_id}", "BookController@updateBook");
Route::delete("/book/{book_id}", "BookController@deleteBook");
Route::put("/book/{book_id}/status", "BookController@changeStatus");


Route::get("/categories", "CategoryController@getCategories");
Route::post("/category", "CategoryController@postCategory");

