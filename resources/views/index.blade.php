@extends('layouts.app')

@section('title', 'Books')


@section('content')

<div class="container">

    <div class="add">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal">
            Add new book
        </button>
    </div>

    @if(count($books) > 0)
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Cuthor</th>
                <th>Category</th>
                <th>Published Date</th>
                <th>Available</th>
                <th></th>
            </tr>
        </thead>


        <tbody>

            @foreach($books as $book)
            <tr>
                <td>{{$book->name}}</td>
                <td>{{$book->author}}</td>
                <td>{{$book->category_name}}</td>
                <td>{{$book->published_date}}</td>
                @if($book->available == false)
                <td>No</td>
                @else
                <td>Yes</td>
                @endif
                <td>
                    <a href="#" class="btn btn-primary btn-sm">Update</a>
                    <a href="#" class="btn btn-danger btn-sm">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>No books has been found</p>
    @endif
</div>


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form action="">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



@endsection


@section("js")

<script>

</script>

@endsection